﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace scientific_games_problem1
{
    class Human
    {
        //public readonly string firstName;
        //public readonly string lastName;
        public readonly int birthDate;
        public readonly int deathDate;

        public Human(int birthDate, int deathDate)
        {
            //this.firstName = firstName;
            //this.lastName = lastName;
            this.birthDate = birthDate;
            this.deathDate = deathDate;
        }
    }
}
