﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace scientific_games_problem1
{
    [TestFixture]
    class TestProgram
    {

        [Test]
        public void Test_PeopleData()
        {
            var res = new List<int> { 1933, 1936, 1937 };
            Assert.AreEqual(res, new Program().OpenExcelAndFindYear("PeopleData.xlsx"));
        }

        [Test]
        public void Test_Empty()
        {
            Assert.AreEqual(null, new Program().OpenExcelAndFindYear(""));
        }
    }
}
