﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace scientific_games_problem1
{
    /// <summary>
    /// Given a list of people with their birth and end years (all between 1900 and 2000),
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            List<int> res = new Program().OpenExcelAndFindYear("PeopleData.xlsx");
            if (res != null)
            {
                Console.Write("Years with most people's quantity alived: ");
                foreach (int item in res)
                    Console.Write(item + " ");
            }
            Console.WriteLine();
        }

        /// <summary>
        /// Opens Excel file and returns year/years with most number of people alive
        /// </summary>
        /// <param name="fileName">name of excel file</param>
        /// <returns>list of years</returns>
        /// <remarks>excel files must be located at projectDir/bin/Debug/Data</remarks>
        public List<int> OpenExcelAndFindYear(string fileName)
        {
            Excel.Application xlApp = new Excel.Application();

            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Data\", fileName);
            if (!File.Exists(path))
            {
                Console.WriteLine("File does not exist: " + path);
                return null;
            }

            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(path);
            Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Excel.Range xlRange = xlWorksheet.UsedRange;

            int rowCount = xlRange.Rows.Count;
            int colCount = xlRange.Columns.Count;
            var listPeople = new List<Human>();

            //excel isn't zero based, and we don't need first row, because it contains name of rows
            //That's why start from 2
            for (int i = 2; i <= rowCount; i++)
            {
                int birthDate = 0;
                int deathDate = 0;

                //name and last name aren't needed, because we don't use them in this problem solution
                for (int j = 3; j <= colCount; j++)
                {
                    Excel.Range cell = xlRange.Cells[i, j];
                    if (cell == null || cell.Value2 == null)
                        continue;

                    //born column
                    if (j == 3)
                        Int32.TryParse(cell.Value2.ToString(), out birthDate);

                    //die column
                    else if (j == 4)
                        Int32.TryParse(cell.Value2.ToString(), out deathDate);
                }

                listPeople.Add(new Human(birthDate, deathDate));
            }

            //cleanup
            GC.Collect();
            GC.WaitForPendingFinalizers();

            Marshal.ReleaseComObject(xlRange);
            Marshal.ReleaseComObject(xlWorksheet);

            xlWorkbook.Close();
            Marshal.ReleaseComObject(xlWorkbook);

            xlApp.Quit();
            Marshal.ReleaseComObject(xlApp);

            return FindYear(listPeople, 1900, 2000);
        }

        /// <summary>
        ///  Find the last year with the most number of people alive.
        /// </summary>
        private List<int> FindYear(List<Human> people, int limitStart, int limitEnd)
        {
            int mostLiveYear = 0;
            int maxLiveCounter = 0;

            var listMostLiveYears = new List<int[]>();
            for (int i = limitStart; i <= limitEnd; i++)
            {
                int counter = 0;
                foreach (Human item in people)
                {
                    if (item.birthDate <= i && item.deathDate >= i)
                        counter++;
                }

                //find the maxLiveCounter
                if (counter >= maxLiveCounter && counter > 0)
                {
                    mostLiveYear = i;
                    maxLiveCounter = counter;
                    listMostLiveYears.Add(new int[2] { maxLiveCounter, mostLiveYear });
                }
            }

            Console.WriteLine("Max alived people's quantity in one year is: " + maxLiveCounter + " from "
                + people.Count + " people");

            //Create list, which will contain years that has final maxLiveCounter
            List<int> listYearsWithMaxCount = new List<int>(listMostLiveYears.Count);
            foreach (int[] item in listMostLiveYears)
            {
                if (item[0] == maxLiveCounter)
                    listYearsWithMaxCount.Add(item[1]);
            }

            return listYearsWithMaxCount;
        }
    }

}


