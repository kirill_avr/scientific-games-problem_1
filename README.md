###Scientific Games: Problem#1

**Description**
 - Given a list of people with their birth and end years (all between 1900 and 2000), find the year with the most number of people alive.

Project contains excel sheets, that represents datasets (*scientific_games_problem1/bin/Debug/Data*)

Example of programs output:

[![Program's output](https://i.imgur.com/ezXDhGf.png "Program's output")](https://i.imgur.com/ezXDhGf.png"Program's output")
